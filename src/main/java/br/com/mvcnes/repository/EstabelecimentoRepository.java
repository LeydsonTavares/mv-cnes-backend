package br.com.mvcnes.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.mvcnes.model.Estabelecimento;


/**
 * @author leydson.ryan.tavares
 *
 */
@Repository
public interface EstabelecimentoRepository extends JpaRepository<Estabelecimento, String>{
	
	List<Estabelecimento> findByTipoGestao(String tipo);
	
	List<Estabelecimento> findAllBySiglaEstado(String sigla);

}