package br.com.mvcnes.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.mvcnes.model.Estabelecimento;
import br.com.mvcnes.repository.EstabelecimentoRepository;

/**
 * @author leydson.ryan.tavares
 *
 */
@Service
public class EstabelecimentoService {

	@Autowired
	private EstabelecimentoRepository estabelecimentoRepository;

	public List<Estabelecimento> listarEstabelecimentos() {
		return estabelecimentoRepository.findAll();
	}

	public List<Estabelecimento> listarEstabelecimentoPorEstado(String sigla) {
		return estabelecimentoRepository.findAllBySiglaEstado(sigla);

	}

	public List<Estabelecimento> listarEstabelecimentoPorTipo(String tipo) {
		return estabelecimentoRepository.findByTipoGestao(tipo);
	}

	public void criarEstabelecimentos(Estabelecimento estabelecimento) {
		estabelecimentoRepository.save(estabelecimento);

	}

}
