package br.com.mvcnes.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author leydson.ryan.tavares
 *
 */
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "estabelecimento")
public class Estabelecimento {

	@Id
	@Getter
	@Column(name = "CO_CNES")
	private long codigoCNES;

	@Getter @Setter
	@Column(name = "CO_IBGE")
	private String codigoIBGE;

	@Getter @Setter
	@Column(name = "NO_FANTASIA")
	private String nomeFantasia;

	@Getter @Setter
	@Column(name = "DS_TIPO_UNIDADE")
	private String descricaoTipoUnidade;

	@Getter @Setter
	@Column(name = "TP_GESTAO")
	private String tipoGestao;

	@Getter @Setter
	@Column(name = "NO_LOGRADOURO")
	private String nomeLogradouro;

	@Getter @Setter
	@Column(name = "NU_ENDERECO")
	private String numeroEndereco;

	@Getter @Setter
	@Column(name = "NO_BAIRRO")
	private String nomeBairro;

	@Getter @Setter
	@Column(name = "CO_CEP")
	private String codigoCEP;

	@Getter @Setter
	@Column(name = "UF")
	private String siglaEstado;

	@Getter @Setter
	@Column(name = "MUNICIPIO")
	private String municipio;

	@Getter @Setter
	@Column(name = "NU_TELEFONE")
	private String numeroTelefone;

	
}