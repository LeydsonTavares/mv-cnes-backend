package br.com.mvcnes.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.mvcnes.exception.ResourceNotFoundException;
import br.com.mvcnes.model.Estabelecimento;
import br.com.mvcnes.service.EstabelecimentoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author leydson.ryan.tavares
 *
 */
@RestController
@Api(value = "Estabelecimento")
@RequestMapping("/api/v1/estabelecimentos")
public class EstabelecimentoController {

	@Autowired
	private EstabelecimentoService estabelecimentoService;

	@ApiOperation(value = "Recupera todos os estabelecimentos do Cadastro Nacional")
	@ApiResponses(value = {
		    @ApiResponse(code = 200, message = "[SUCESSO] Retorna estabelecimentos do Cadastro Nacional"),
		    @ApiResponse(code = 403, message = "Você não tem permissão para acessar este servidor"),
		    @ApiResponse(code = 404, message = "Não encontrado, não existe estabelecimentos."),
		    @ApiResponse(code = 400, message = "Solicitação incorreta, formato inválido da solicitação."),
		    @ApiResponse(code = 422, message = "Entidade não processável, parâmetros de entrada causaram falha no processamento."),
		    @ApiResponse(code = 500, message = "Erro Interno do Servidor"),
		})
	@GetMapping("/")
	public ResponseEntity<List<Estabelecimento>> getEstabelecimentos() {
		List<Estabelecimento> estabelecimentos = estabelecimentoService.listarEstabelecimentos();

		try {
			if (estabelecimentos.isEmpty()) {
				throw new ResourceNotFoundException("Employee not found for this");
			}

			return new ResponseEntity<List<Estabelecimento>>(estabelecimentos, HttpStatus.OK);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(estabelecimentos);
		}
	}
	@ApiOperation(value = "Recupera estabelecimentos do CNES filtrando por Estado")
	@ApiResponses(value = {
		    @ApiResponse(code = 200, message = "[SUCESSO] Retorna a lista de CNES por Estado"),
		    @ApiResponse(code = 403, message = "Você não tem permissão para acessar este servidor"),
		    @ApiResponse(code = 404, message = "Não encontrado, não existe estabelecimentos no estado informado."),
		    @ApiResponse(code = 400, message = "Solicitação incorreta, formato inválido da solicitação."),
		    @ApiResponse(code = 422, message = "Entidade não processável, parâmetros de entrada causaram falha no processamento."),
		    @ApiResponse(code = 500, message = "Erro Interno do Servidor"),
		})
	@GetMapping("/{sigla}")
	public ResponseEntity<List<Estabelecimento>> getEstabelecimentoPorEstado(@Valid @PathVariable String sigla) {
		List<Estabelecimento> estabelecimentos = estabelecimentoService.listarEstabelecimentoPorEstado(sigla);

		try {
			if (estabelecimentos.isEmpty()) {
				throw new ResourceNotFoundException("Employee not found for this id :: " + sigla);
			}

			return new ResponseEntity<List<Estabelecimento>>(estabelecimentos, HttpStatus.OK);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(estabelecimentos);
		}
	}

	@ApiOperation(value = "Recupera estabelecimentos do CNES filtrando por Tipo de Gestão (E - Estadual M - Municipal D - Dupla)")
	@ApiResponses(value = {
		    @ApiResponse(code = 200, message = "[SUCESSO] Retorna a lista de CNES por Tipo de Gestão"),
		    @ApiResponse(code = 403, message = "Você não tem permissão para acessar este servidor"),
		    @ApiResponse(code = 404, message = "Não encontrado, não existe estabelecimentos para o tipo de Gestão informado."),
		    @ApiResponse(code = 400, message = "Solicitação incorreta, formato inválido da solicitação."),
		    @ApiResponse(code = 422, message = "Entidade não processável, parâmetros de entrada causaram falha no processamento."),
		    @ApiResponse(code = 500, message = "Erro Interno do Servidor"),
		})
	@GetMapping("/gestao/{tipo}")
	public ResponseEntity<List<Estabelecimento>> getEstabelecimentoPorTipo(@Valid @PathVariable String tipo) {
		List<Estabelecimento> estabelecimentos = estabelecimentoService.listarEstabelecimentoPorTipo(tipo);

		try {
			if (estabelecimentos.isEmpty()) {
				throw new ResourceNotFoundException("Employee not found for this id :: " + tipo);
			}
			return new ResponseEntity<List<Estabelecimento>>(estabelecimentos, HttpStatus.OK);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(estabelecimentos);
		}
	}

	@ApiOperation(value = "Criar novo estabelecimento")
	@ApiResponses(value = {
		    @ApiResponse(code = 201, message = "[SUCESSO] Estabelecimento adicionado"),
		    @ApiResponse(code = 403, message = "Você não tem permissão para acessar este servidor"),
		    @ApiResponse(code = 400, message = "Solicitação incorreta, formato inválido da solicitação."),
		    @ApiResponse(code = 422, message = "Entidade não processável, parâmetros de entrada causaram falha no processamento."),
		    @ApiResponse(code = 500, message = "Erro Interno do Servidor"),
		})
	@PostMapping("/")
	public ResponseEntity<Object> createEstabelecimento(@Valid @RequestBody Estabelecimento estabelecimento) {
		try {
			estabelecimentoService.criarEstabelecimentos(estabelecimento);
			return new ResponseEntity<Object>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Object>(HttpStatus.BAD_REQUEST);
		}
	}
}