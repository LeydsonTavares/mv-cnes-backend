


<div  align="center" style="margin: 35px">
    <img width="500" src="https://spring.io/images/spring-logo-9146a4d3298760c2e7e49595184e1975.svg">
</div>


---

### Desafio MV 

Utilizando boas práticas de desenvolvimento no software, crie uma aplicação para ler e apresentar dados do Cadastro Nacional de Estabelecimentos de Saúde (CNES) no Brasil.
Fonte dos dados: http://dados.gov.br/dataset/cnes_ativo

####  mv-cnes-back-end

API desenvolvida com Spring Boot, PostgreSql, JPA e Swagger.

Link de acesso ao Swagger

> https://mv-cnes-backend.herokuapp.com/swagger-ui.html#

---

#### Passo 1 - Importando Projeto

> Importar Projeto Maven em sua IDE de preferência

#### Passo 2 - Start o projeto

> Realizar update das dependências do Maven

#### Hospedagem - Heroku

Heroku é uma plataforma na nuvem que faz deploy de várias aplicações Back-end e Front-end seja para hospedagem, testes em produção ou escalar as suas aplicações. Também tem integração com o GitHub, deixando o uso mais fácil e com containers denominados Dyno.

Para aplicação `mv-cnes-backend` foi utilizado um `Free Dynos` ideal para experimentar aplicativos na nuvem em uma sandbox limitada que dorme após 30 min de inatividade.


Domínio

> https://mv-cnes-backend.herokuapp.com/api/v1/estabelecimentos/


#### Limitações

Por questões de limitações do plano `Free Dynos` o banco de dados `Heroku Postgres` só possibilita registro de 10.000 linhas e 20 conexões simultânea. Sendo assim foram inseridos os primeiros 10.0000 registros das CNES, extraídos da fonte: http://dados.gov.br/dataset/cnes_ativo e efetuado insert na tabela "ESTABELECIMENTOS".

